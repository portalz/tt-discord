import datetime
import discord
import asyncio
import requests
from redbot.core import commands
from redbot.core import checks
from redbot.core import Config


def checkImageAuthorization(filename):
    imageauth = requests.get('https://ttwiki.transformation.tf/api/cog/imageauth', params={'file': filename})
    if imageauth.status_code == 200:
        return True
    else:
        return False

    # Listener stuff


listener = getattr(commands.Cog, "listener", None)  # red 3.0 backwards compatibility support
if listener is None:  # thanks Sinbad
    def listener(name=None):
        return lambda x: x


class ttwiki(commands.Cog):

    # @commands.command()
    # async def ttsearch(self, ctx, *, query):
    #     """Searches TT-wiki"""
    # types = ["effect", "form", "furniture", "item", "location", "spell"]
    # if (query.find(":") > 0):
    #     query_parts = query.split(':', 1)
    #     if query_parts[0] in types:  # This query specifies a type, search specifically for that type
    #         payload = {'type': query_parts[0], 'q': query_parts[1]}
    #         r = requests.get('https://ttwiki.transformation.tf/api/cog/search', params=payload)
    #         results=r.json()
    #         for result in results:
    #             print(result)
    #     else:
    #         payload = {'q': query}
    #         r = requests.get('https://ttwiki.transformation.tf/api/cog/search', params=payload)
    # else:
    #     payload = {'q': query}
    #     r = requests.get('https://ttwiki.transformation.tf/api/cog/search', params=payload)

    @commands.command()
    async def ttinfo(self, ctx, *, query):
        types = ["effect", "form", "furniture", "item", "location", "spell"]
        stats = ["Discipline", "Perception", "Charisma", "Fortitude", "Agility", "Allure", "Magicka", "Succour", "Luck"]
        special_stats = ["InstantHealthRestore", "InstantManaRestore", "ReuseableHealthRestore", "ReuseableManaRestore"]
        if (query.find(":") > 0):
            query_parts = query.split(':', 1)
            type = query_parts[0]
            if type in types:  # This query specifies a type, search specifically for that type
                payload = {'type': type, 'q': query_parts[1]}
                r = requests.get('https://ttwiki.transformation.tf/api/cog/lookup', params=payload)
                if (r.status_code == 200):
                    content = r.json()
                    if (len(content) == 0):
                        await ctx.send("no result was returned")
                        return
                    if (type == "effect"):
                        embed = discord.Embed(title=str(content["FriendlyName"]),
                                              description=str(content["Description"]), color=0x734752)
                        embed.add_field(name="Id", value=content["Id"])
                        if (content["ObtainedAtLocation"] != None):
                            embed.add_field(name="Obtained at Location", value=content["ObtainedAtLocation"])
                        if (content["AvailableAtLevel"] != 0):
                            embed.add_field(name="Minimum Level Requirement", value=content["AvailableAtLevel"])
                        embed.add_field(name="Level-Up Perk",
                                        value="true" if content["isLevelUpPerk"] == 1 else "false")
                        embed.add_field(name="Removable",
                                        value="true" if content["IsRemovable"] == 1 else "false")
                        if (content["PreRequisiteEffectSourceId"] != 0):
                            embed.add_field(name="Requires Effect", value=content["PreRequisiteEffectSourceId"])
                        if (content["Duration"] != 0):
                            embed.add_field(name="Duration", value=content["Duration"])
                        if (content["Cooldown"] != 0):
                            embed.add_field(name="Cooldown", value=content["Cooldown"])
                        for stat in stats:
                            if (content[stat] != "0.000"):
                                embed.add_field(name=stat, value=content[stat])
                        embed.add_field(name="link",value='https://ttwiki.transformation.tf/wiki/wiki.php?type=effect&id='+str(content["Id"]))

                        await ctx.send(embed=embed)
                    if (type == "form"):
                        embed = discord.Embed(title=str(content["FriendlyName"]),
                                              description=str(content["Description"]), color=0x734752)
                        embed.add_field(name="Id", value=content["Id"])
                        embed.add_field(name="Gender", value=content["Gender"])
                        embed.add_field(name="Image", value=content["PortraitUrl"])
                        if (checkImageAuthorization(content["PortraitUrl"])):
                            embed.set_thumbnail(url="https://cdn.ttwiki.transformation.tf/img/" + content["PortraitUrl"])
                        embed.add_field(name="link",value='https://ttwiki.transformation.tf/wiki/wiki.php?type=form&id='+str(content["Id"]))

                        await ctx.send(embed=embed)
                    if (type == "furniture"):
                        embed = discord.Embed(title=str(content["FriendlyName"]), color=0x734752)
                        embed.add_field(name="Id", value=content["Id"])
                        embed.add_field(name="dbType", value=content["dbType"])
                        embed.add_field(name="Cost", value=content["BaseCost"])
                        embed.add_field(name="Contract Turn Length", value=content["BaseContractTurnLength"])
                        embed.add_field(name="Base Cost", value=content["BaseCost"])
                        if (content["APReserveRefillAmount"] != "0.000"):
                            embed.add_field(name="AP Reserve Refill", value=content["APReserveRefillAmount"])
                        if (content["GivesEffectSourceId"] != None):
                            embed.add_field(name="Gives Effect ID", value=content["GivesEffectSourceId"])
                        if (content["GivesItemSourceId"] != None):
                            embed.add_field(name="Gives Item ID", value=content["GivesItemSourceId"])
                        embed.add_field(name="link",value='https://ttwiki.transformation.tf/wiki/wiki.php?type=furniture&id='+str(content["Id"]))

                        await ctx.send(embed=embed)
                    if (type == "item"):
                        embed = discord.Embed(title=str(content["FriendlyName"]),
                                              description=str(content["Description"]), color=0x734752)
                        embed.add_field(name="Id", value=content["Id"])
                        embed.add_field(name="Type", value=content["ItemType"])
                        if (int(content["UseCooldown"]) > 0):
                            embed.add_field(name="Cooldown", value=content["UseCooldown"])
                        embed.add_field(name="Findable",
                                        value="true" if content["Findable"] == 1 else "false")
                        embed.add_field(name="Image", value=content["PortraitUrl"])
                        if (content["GivesEffectSourceId"] != None):
                            embed.add_field(name="Gives effect", value=content["GivesEffectSourceId"])
                        if (content["CurseTFFormSourceId"] != None):
                            embed.add_field(name="TF Curse Form Id:", value=content["CurseTFFormSourceId"])
                        if (content["ItemType"] == "rune"):
                            for stat in stats:
                                if (content[stat] != "0.000"):
                                    embed.add_field(name=stat, value=content[stat])
                        if (content["ItemType"] == "consumable_reuseable"):
                            for stat in special_stats:
                                if (content[stat] != "0"):
                                    embed.add_field(name=stat, value=content[stat])
                        if (checkImageAuthorization(content["PortraitUrl"])):
                            embed.set_thumbnail(url="https://cdn.ttwiki.transformation.tf/img/" + content["PortraitUrl"])
                        embed.add_field(name="link",value='https://ttwiki.transformation.tf/wiki/wiki.php?type=item&id='+str(content["Id"]))
                        await ctx.send(embed=embed)
                    if (type == "location"):
                        embed = discord.Embed(title=content["Name"], color=0x734752)
                        embed.add_field(name="Id", value=content["Id"])
                        embed.add_field(name="X", value=content["X"])
                        embed.add_field(name="Y", value=content["Y"])
                        if (content["Region"] != None and content["Region"] != ""):
                            embed.add_field(name="Region", value=content["Region"])
                        if (content["FriendlyName_North"] != None and content["FriendlyName_North"] != ""):
                            embed.add_field(name="North", value=content["FriendlyName_North"])
                        if (content["FriendlyName_East"] != None and content["FriendlyName_East"] != ""):
                            embed.add_field(name="East", value=content["FriendlyName_East"])
                        if (content["FriendlyName_South"] != None and content["FriendlyName_South"] != ""):
                            embed.add_field(name="South", value=content["FriendlyName_South"])
                        if (content["FriendlyName_West"] != None and content["FriendlyName_West"] != ""):
                            embed.add_field(name="West", value=content["FriendlyName_West"])
                        await ctx.send(embed=embed)
                    if (type == "spell"):
                        embed = discord.Embed(title=str(content["FriendlyName"]),
                                              description=str(content["Description"]), color=0x734752)
                        embed.add_field(name="Id", value=content["Id"])
                        embed.add_field(name="FormSourceId", value=content["FormSourceId"])
                        if (content["LearnedAtRegion"] != ""):
                            embed.add_field(name="LearnedAtRegion", value=content["LearnedAtRegion"])
                        if (content["LearnedAtLocation"] != ""):
                            embed.add_field(name="LearnedAtLocation", value=content["LearnedAtLocation"])
                        embed.add_field(name="MobilityType", value=content["MobilityType"])
                        embed.add_field(name="link",value='https://ttwiki.transformation.tf/wiki/wiki.php?type=spell&id='+str(content["Id"]))
                        await ctx.send(embed=embed)


                else:
                    await ctx.send(
                        "Sorry, that's not a valid command. Please use `;ttinfo item:latex bra` or `;ttinfo item:33`.")
                    return
            else:
                await ctx.send(
                    "Sorry, that's not a valid command. Please use `;ttinfo item:latex bra` or `;ttinfo item:33`.")
                return
        else:
            await ctx.send(
                "Sorry, that's not a valid command. Please use `;ttinfo item:latex bra` or `;ttinfo item:33`.")
            return
