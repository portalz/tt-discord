import datetime
import discord
import asyncio

from redbot.core import commands
from redbot.core import checks
from redbot.core import Config

# Listener stuff
listener = getattr(commands.Cog, "listener", None)  # red 3.0 backwards compatibility support
if listener is None:  # thanks Sinbad
    def listener(name=None):
        return lambda x: x


class gatekeeper(commands.Cog):

    # Listens for posts in #lobby
    @listener()
    async def on_message(self, message):
        # Is this message in a guild?
        if (message.guild):
            guild = message.guild
            if (guild.id == 169683982793965569):
                msg_channel = guild.get_channel(message.channel.id)
                mod_channel = guild.get_channel(473706854120423446)
                lobby_channel = guild.get_channel(415640743667957760)
                verified_role = self.role_from_id(415640273243209738)

                if (msg_channel.id == lobby_channel.id):
                    # print(message.content)
                    # await channel.send("Message in"+lobby_channel.name+"")
                    if ((message.author.id != 389645222918881280) and (message.content)): #verify that the message is not by the bot and that the message is not empty
                        if ((str(message.content) == "I have read and agree to the rules.")): # the message matches the configured text
                            if (message.author.joined_at < datetime.utcnow() - timedelta(minutes=-1)): # used joined more than one minute ago


                            else: #user joined less than one minute ago
                                await message.author.send(
                                    "Hi, " + message.author.mention + ", you seem to have been a little quick there. Make sure you've read the rules and try again.")

                        # embed = discord.Embed(title="Lobby Message", description=discord.utils.escape_mentions(discord.utils.escape_markdown(message.content)), color=0x734752)
                        # embed.add_field(name="Timestamp", value=str(datetime.datetime.now()))
                        # embed.add_field(name="Author", value=message.author.name+"#"+str(message.author.discriminator))
                        # embed.add_field(name="Author ID", value=str(message.author.id))
                        # embed.add_field(name="Message ID", value=str(message.id))
                        # await mod_channel.send(embed=embed)
























    # Listener for when members join the guild.
    @listener()
    async def on_member_join(self, member):
        guild = member.guild
        mod_channel = guild.get_channel(473706854120423446)
        if (guild.id == 169683982793965569):
            embed = discord.Embed(title="User joined the guild", description=str(member.mention) + " joined the guild.",
                                  color=0x28a728)
            embed.add_field(name="Timestamp", value=str(datetime.datetime.now()))
            embed.add_field(name="Member", value=member.name + "#" + str(member.discriminator))
            embed.add_field(name="Member ID", value=str(member.id))
            await mod_channel.send(embed=embed)

    # Listener for when members leave the guild
    @listener()
    async def on_member_remove(self, member):
        guild = member.guild
        mod_channel = guild.get_channel(473706854120423446)
        if (guild.id == 169683982793965569):
            embed = discord.Embed(title="User left the guild", description=str(member.mention) + " left the guild.",
                                  color=0xdc3545)
            embed.add_field(name="Timestamp", value=str(datetime.datetime.now()))
            embed.add_field(name="Member", value=member.name + "#" + str(member.discriminator))
            embed.add_field(name="Member ID", value=str(member.id))
            await mod_channel.send(embed=embed)
