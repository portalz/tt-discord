
import discord
import asyncio

from redbot.core import commands
from redbot.core import checks

#Listener stuff
listener = getattr(commands.Cog, "listener", None)  # red 3.0 backwards compatibility support
if listener is None:  # thanks Sinbad
    def listener(name=None):
        return lambda x: x


# Stuff that does stuff
class ttwelcome(commands.Cog):
    #The Welcome! Listener
    @listener()
    async def on_member_update(self, before, after):
        guild = before.guild
        if guild.id==169683982793965569:
            channel=guild.get_channel(169683982793965569)
            before_attr = getattr(before, "roles")
            after_attr = getattr(after, "roles")
            if before_attr != after_attr:
                b = set(before.roles)
                a = set(after.roles)
                before_roles = [list(b - a)][0]
                after_roles = [list(a - b)][0]
                if after_roles:
                    for role in after_roles:
                        if role.id==415640273243209738:
                            await channel.send(f"Welcome {before.mention}! <:lindella:588895772800450587>")
