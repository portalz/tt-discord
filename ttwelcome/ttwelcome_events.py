import datetime
import discord
import asyncio

from redbot.core import commands
listener = getattr(commands.Cog, "listener", None)  # red 3.0 backwards compatibility support

class ttwelcome_events():
    """
        Handles all the on_event data
    """

    def __init__(self, *args):
        self.config: Config
        self.bot: Red

    @commands.Cog.listener()
    async def on_member_update(self, before, after):
        guild = before.guild
        if guild.id is not "169683982793965569":
            return
        channel=guild.get_channel(169683982793965569)
        time = datetime.datetime.utcnow()
        member_updates = {"roles": _("Roles:")}
        for attr, name in member_updates.items():
            before_attr = getattr(before, attr)
            after_attr = getattr(after, attr)
            if before_attr != after_attr:
                if attr == "roles":
                    b = set(before.roles)
                    a = set(after.roles)
                    before_roles = [list(b - a)][0]
                    after_roles = [list(a - b)][0]
                    if after_roles:
                        for role in after_roles:
                            if(role.id is 415640273243209738):
                                await channel.send("Welcome @"+f"{before.name}#{before.discriminator}!")
