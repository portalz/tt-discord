import discord
from redbot.core import commands
from redbot.core import checks
from redbot.core.utils.mod import slow_deletion, mass_purge
from redbot.core.utils.predicates import MessagePredicate
from .converters import RawMessageIds

from datetime import datetime, timedelta
from typing import Union, List, Callable, Set


class Janitor(commands.Cog):
    @staticmethod
    async def get_messages_for_deletion(
            *,
            channel: discord.TextChannel,
            number: int = None,
            check: Callable[[discord.Message], bool] = lambda x: True,
            before: Union[discord.Message, datetime] = None,
            after: Union[discord.Message, datetime] = None,
            delete_pinned: bool = False,
    ) -> List[discord.Message]:

        # This isn't actually two weeks ago to allow some wiggle room on API limits
        two_weeks_ago = datetime.utcnow() - timedelta(days=14, minutes=-5)
        one_day_ago = datetime.utcnow() - timedelta(days=1)

        def message_filter(message):
            return (
                    check(message)
                    and message.created_at > two_weeks_ago
                    and (delete_pinned or not message.pinned)
                    and message.created_at < one_day_ago
            )

        if after:
            if isinstance(after, discord.Message):
                after = after.created_at
            after = max(after, two_weeks_ago)

        collected = []
        async for message in channel.history(
                limit=None, before=before, after=after, oldest_first=False
        ):
            if message.created_at < two_weeks_ago:
                break
            if message_filter(message):
                collected.append(message)
                if number and number <= len(collected):
                    break

        return collected

    @commands.command()
    @commands.guild_only()
    @checks.mod()
    @commands.bot_has_permissions(manage_messages=True)
    async def janitor(
            self, ctx: commands.Context, message_id: RawMessageIds, delete_pinned: bool = False
    ):
        """Deletes all messages after a key messsages older than 24 hours and newer than 14 days.
        """

        channel = ctx.channel
        author = ctx.author

        try:
            after = await channel.fetch_message(message_id)
        except discord.NotFound:
            return await ctx.send("Message not found.")

        to_delete = await self.get_messages_for_deletion(
            channel=channel, number=None, after=after, delete_pinned=delete_pinned
        )
        # test
        await mass_purge(to_delete, channel)
