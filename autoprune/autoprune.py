from redbot.core import commands
from redbot.core import checks

class Autoprune(commands.Cog):
    @commands.command()
    @checks.mod()
    async def qxa(self,ctx):
        await ctx.send("awoo")
        await ctx.message.delete();

    @commands.command()
    @checks.mod()
    async def qxm(self,ctx):
        await ctx.send("moo")
        await ctx.message.delete();

    @commands.command()
    @checks.mod()
    async def qxn(self,ctx):
        await ctx.send("nya")
        await ctx.message.delete();

    @commands.command()
    @commands.guild_only()
    @checks.mod()
    async def prune(self, ctx, time: int = 7):
        """Prunes the server. By default, it prunes all users who have been inactive for the past 7 days."""
        pruned = await ctx.guild.prune_members(days=time)
